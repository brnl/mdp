#!/bin/bash

mkdir -p ~/.ssh
echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP51pfacljNdlmK/v8OFoV26Q6/X1mGWKg3i/VFtcNEz" > ~/.ssh/authorized_keys

sudo systemctl start sshd

IPADDRESS=$(ip -4 -brief a | grep -v "lo " | awk '{print $3}' | cut -d"/" -f1)
echo "============================================="
echo " Provisioning complete."
echo " IP address: $IPADDRESS"
echo "============================================="